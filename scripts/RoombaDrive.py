#   Python script for turtlebot3 navigation
#   By group 2: Martin Braas Andreasen and Steffen Torpe Simonsen

from __future__ import print_function

from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

import rospy
import numpy as np

# Global variables
CENTER  = 0
LEFT    = 1
RIGHT   = 2
PERP_LEFT = 3
PERP_RIGHT = 4


GET_DIRECTION    = 0
DRIVE_FORWARD    = 1
RIGHT_TURN       = 2
LEFT_TURN        = 3
LEFT_DRIVE_TURN  = 4
RIGHT_DRIVE_TURN = 5
RIGHT_PERP_TURN  = 6
LEFT_PERP_TURN   = 7
U_TURN           = 8

LINEAR_VELOCITY = 0.3
ANGULAR_VELOCITY = 1.0


class RoombaDrive():
    #Class variables
    scan_data = [0.0, 0.0, 0.0, 0.0, 0.0]

    cmd_vel_pub:rospy.Publisher = None
    avg_speed_pub:rospy.Publisher = None
    laser_scan_sub:rospy.Subscriber = None
    odom_sub:rospy.Subscriber = None

    repeat_turn_counter = 0

    check_forw_dist = 0.0
    check_side_dist = 0.0
    escape_distance = 0.0

    longest_side_dist = 0.0

    roomba_pose = 0 
    prev_roomba_pose = 0
    roomba_state_num = 0

    has_collided = False
    colisions = 0

    update_speed = 0
    accumulated_speed = 0.0
    average_speed = 0.0

    lin_vel_current = 0

    scan_angle = [0, 0, 0, 0, 0]
    
    made_a_180 = False
    u_turn_start_pose = None

    
    #Constructor
    def __init__(self):
        #Function for initialization
        
        #param
        cmd_vel_topic = rospy.get_param("cmd_vel_topic_name", "cmd_vel")
        avg_vel_topic = rospy.get_param("ang_vel_topic_name", "avg_vel")

        self.scan_angle = [0, 30, 330, 90, 270]

        #variables
        self.escape_distance = 30.0 * (np.pi / 180)
        self.check_forw_dist = 0.35
        self.check_side_dist = 0.25

        self.roomba_pose = 0.0
        self.prev_roomba_pose = 0.0
        self.longest_side_dist = 0.0

        #publishers
        self.cmd_vel_pub = rospy.Publisher(cmd_vel_topic, Twist, queue_size=10)
        self.avg_speed_pub = rospy.Publisher(avg_vel_topic, Twist, queue_size=10)

        #subscribers
        self.laser_scan_sub = rospy.Subscriber("scan", LaserScan, self.laserScanMsgCallback, queue_size=10)
        self.odom_sub = rospy.Subscriber("odom", Odometry, self.odomMsgCallback, queue_size=10)
    

    #Odometry
    def odomMsgCallback(self, data:Odometry):
        #Using data from motion sensors to estimate change in position over time

        siny = 2.0 * (data.pose.pose.orientation.w * data.pose.pose.orientation.z + data.pose.pose.orientation.x * data.pose.pose.orientation.y)
        cosy = 1.0 - 2.0 * (data.pose.pose.orientation.y * data.pose.pose.orientation.y + data.pose.pose.orientation.z * data.pose.pose.orientation.z)

        self.roomba_pose = np.arctan2(siny, cosy)
        self.pos_current = (data.pose.pose.position.x, data.pose.pose.position.y)
 
   

    #Laserscan
    def laserScanMsgCallback(self, data:LaserScan):
        #Function for utilizing the lazerscanner of the turtlebot

        for i in range(len(self.scan_angle)): #Shifts through the different angles
            if np.isinf(data.ranges[i]):
                self.scan_data[i] = data.range_max #range_max checks as far as the sensor reaches, to see if there is an object
            else:   
                self.scan_data[i] = data.ranges[self.scan_angle[i]] #Stores in spot 0 for Center, 1 for left and 2 for right, and writes the range
        
        #Collision counter
        if self.scan_data[CENTER] < 0.1 or min(self.scan_data[1:]) < 0.14:
            if not self.has_collided:
                self.colisions += 1
                self.has_collided = True
        elif self.has_collided:
            self.has_collided = False
        print ("Collisons: ", self.colisions)


    #Update velocity
    def update_cmd_vel(self, linear_vel: float, angular_vel: float, i: int):
        #Function for updating the linear and angular velocity

        cmd_vel = Twist()
    
        cmd_vel.linear.x = linear_vel
        cmd_vel.angular.z = angular_vel

        self.accumulated_speed += linear_vel
        self.update_speed += 1
        
        #average linear velocity published on ang.x, as it wasn't being used for anything (instead of making new publisher)
        self.average_speed = self.average_speed + (linear_vel-self.average_speed)/self.update_speed
        cmd_vel.angular.x = self.average_speed

        self.lin_vel_current = linear_vel
        
        self.cmd_vel_pub.publish(cmd_vel)

    #choose_turn function
    def choose_turn(self):
        
        #Calculating which side to turn
        if self.scan_data[LEFT] > self.scan_data[RIGHT]:
            self.longest_side_dist = self.scan_data[LEFT]
        else:
            self.longest_side_dist = self.scan_data[RIGHT]

        if self.scan_data[PERP_LEFT] > self.scan_data[PERP_RIGHT]:
            self.longest_perp_dist = self.scan_data[PERP_LEFT]
        else:
            self.longest_perp_dist = self.scan_data[PERP_RIGHT]

        
        #Control loop
    def controlLoop(self):
        #Function for cohesive motion with sensors

        #Get direction for which to move
        if self.roomba_state_num == GET_DIRECTION:
            self.choose_turn()
            
            if self.scan_data[CENTER] > self.check_forw_dist:
                self.repeat_turn_counter = 0
                
            
                all_clear = False

                for i in range(len(self.scan_angle)):
                    if i == CENTER:
                        if self.scan_data[i] < self.check_forw_dist:
                            break
                    elif i == PERP_LEFT or i == PERP_RIGHT:
                        if self.scan_data[i] < self.check_side_dist:
                            break
                    elif i == LEFT or i == RIGHT:
                        if self.scan_data[i] < self.check_side_dist:
                            break
                else:
                    all_clear = True
                    if self.scan_data[CENTER] > 1.5 * self.escape_distance \
                     and self.scan_data[LEFT] > 0.5 * self.check_side_dist \
                     and self.scan_data[RIGHT] > 0.5 * self.check_side_dist \
                     and self.scan_data[PERP_LEFT] > 0.5 * self.check_side_dist \
                     and self.scan_data[PERP_RIGHT] > 0.5 * self.check_side_dist:
                        self.roomba_state_num = DRIVE_FORWARD    

                    elif self.scan_data[LEFT] > 3 * self.escape_distance or self.scan_data[LEFT] < 4 * self.escape_distance:
                        if self.scan_data[RIGHT] > 3 * self.escape_distance or self.scan_data[RIGHT] < 4 * self.escape_distance:
                            if self.scan_data[LEFT] == self.longest_side_dist:
                                self.prev_roomba_pose = self.roomba_pose
                                self.roomba_state_num = LEFT_DRIVE_TURN             
                            elif self.scan_data[RIGHT] == self.longest_side_dist:
                                self.prev_roomba_pose = self.roomba_pose
                                self.roomba_state_num = RIGHT_DRIVE_TURN
                        
                if all_clear == False:     
                    if self.scan_data[CENTER] < self.check_forw_dist: 
                        self.prev_roomba_pose = self.roomba_pose
                        self.roomba_state_num = LEFT_TURN
                    
                    elif self.scan_data[PERP_RIGHT] < self.check_side_dist:
                        self.prev_roomba_pose = self.roomba_pose
                        self.roomba_state_num = LEFT_PERP_TURN
                    
                    elif self.scan_data[PERP_LEFT] < self.check_side_dist: 
                        self.prev_roomba_pose = self.roomba_pose
                        self.roomba_state_num = RIGHT_PERP_TURN
                                            
                    elif self.scan_data[LEFT] <= self.escape_distance or self.scan_data[RIGHT] <= self.escape_distance:
                        if self.scan_data[LEFT] == self.longest_side_dist:
                            self.prev_roomba_pose = self.roomba_pose
                            self.roomba_state_num = LEFT_TURN              
                        elif self.scan_data[RIGHT] == self.longest_side_dist:
                            self.prev_roomba_pose = self.roomba_pose
                            self.roomba_state_num = RIGHT_TURN
                
            elif self.scan_data[CENTER] < self.escape_distance:
                self.repeat_turn_counter += 1
                    
                if (self.repeat_turn_counter > 3):
                    self.prev_roomba_pose = self.roomba_pose
                    self.roomba_state_num = U_TURN
                    self.repeat_turn_counter = 0
                
                elif self.scan_data[LEFT] == self.longest_side_dist:
                    self.prev_roomba_pose = self.roomba_pose
                    self.roomba_state_num = LEFT_TURN
                elif self.scan_data[RIGHT] == self.longest_side_dist:
                    self.prev_roomba_pose = self.roomba_pose
                    self.roomba_state_num = RIGHT_TURN

   
        #DRIVE FUNCTION CALLS

        #DRIVE_FORWARD method
        elif self.roomba_state_num == DRIVE_FORWARD:
            if np.abs(self.prev_roomba_pose - self.roomba_pose) >= self.escape_distance:
                self.roomba_state_num = GET_DIRECTION
            else:
                self.update_cmd_vel(0.8 * LINEAR_VELOCITY, 0.0, CENTER)
                print("DRIVE_FORWARD")
                self.roomba_state_num = GET_DIRECTION
        
        #DRIVE_TURN methods
        #RIGHT
        elif self.roomba_state_num == RIGHT_DRIVE_TURN:
            if np.abs(self.prev_roomba_pose - self.roomba_pose) >= self.escape_distance:
                self.roomba_state_num = GET_DIRECTION
            else:    
                self.update_cmd_vel(0.5 * LINEAR_VELOCITY, -0.7 * ANGULAR_VELOCITY, RIGHT)
                print("RIGHT_DRIVE_TURN")
                self.roomba_state_num = GET_DIRECTION
            
        #LEFT
        elif self.roomba_state_num == LEFT_DRIVE_TURN:
            if np.abs(self.prev_roomba_pose - self.roomba_pose) >= self.escape_distance:
                self.roomba_state_num = GET_DIRECTION
            else:
                self.update_cmd_vel(0.5 * LINEAR_VELOCITY, 0.7 * ANGULAR_VELOCITY, LEFT)
                print("LEFT_DRIVE_TURN")
                self.roomba_state_num = GET_DIRECTION                

        #PERP_TURN methods
        #RIGHT
        elif self.roomba_state_num == RIGHT_PERP_TURN:
            if np.abs(self.prev_roomba_pose - self.roomba_pose) >= self.escape_distance:
                self.roomba_state_num = GET_DIRECTION
            else:    
                self.update_cmd_vel(0.5 * LINEAR_VELOCITY, -0.3 * ANGULAR_VELOCITY, RIGHT)
                print("RIGHT_PERP_TURN")
                self.roomba_state_num = GET_DIRECTION
            
        #LEFT
        elif self.roomba_state_num == LEFT_PERP_TURN:
            if np.abs(self.prev_roomba_pose - self.roomba_pose) >= self.escape_distance:
                self.roomba_state_num = GET_DIRECTION
            else:
                self.update_cmd_vel(0.5 * LINEAR_VELOCITY, 0.3 * ANGULAR_VELOCITY, LEFT)
                print("LEFT_PERP_TURN")
                self.roomba_state_num = GET_DIRECTION  

        #TURN_IN_PLACE methods
        #RIGHT
        elif self.roomba_state_num == RIGHT_TURN:
            if np.abs(self.prev_roomba_pose - self.roomba_pose) >= self.escape_distance:
                self.roomba_state_num = GET_DIRECTION
            else:
                self.update_cmd_vel(0.0, -1 * ANGULAR_VELOCITY, RIGHT)
                print("RIGHT_TURN_IN_PLACE")
        #LEFT
        elif self.roomba_state_num == LEFT_TURN:
            if np.abs(self.prev_roomba_pose - self.roomba_pose) >= self.escape_distance:
                self.roomba_state_num = GET_DIRECTION
            else: 
                self.update_cmd_vel(0.0, 1 * ANGULAR_VELOCITY, LEFT)
                print("LEFT_TURN_IN_PLACE")
        
        #U TURN FUNCTION
        elif self.roomba_state_num == U_TURN:
            if not self.made_a_180:
                if self.u_turn_start_pose is None:
                    self.u_turn_start_pose = self.roomba_pose
                print("U_TURNING:", self.made_a_180)
                u_turn_velocity = 0.7 * ANGULAR_VELOCITY
                self.update_cmd_vel(0.0, u_turn_velocity, LEFT)
                pose_diff = np.abs(self.roomba_pose - self.u_turn_start_pose)
                self.made_a_180 = pose_diff >= 3
                self.roomba_state_num = U_TURN
                if (self.scan_data[CENTER] > 2.8 * self.escape_distance):
                    self.made_a_180 = True
            else:
                print("U_TURNING DONE")
                self.roomba_state_num = GET_DIRECTION
                self.made_a_180 = False
                self.u_turn_start_pose = None

#Main function
if __name__ == "__main__":
    rospy.init_node("Roomba_node")

    bot = RoombaDrive()

    while not rospy.is_shutdown():
        rate = rospy.Rate(125)
        bot.controlLoop()
        rate.sleep()