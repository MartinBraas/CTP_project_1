# Group_2_CTP_project_1

## Command line instructions
You can also upload existing files from your computer using the instructions below.

### Git global setup:
	git config --global user.name "Jhon Doe"
	git config --global user.email "Jdo@gmail.com"

### Create a new repository
	git clone https://gitlab.com/AU_CTP/G4_TB.git
	cd "Destination of choice"
	touch README.md
	git add README.md
	git commit -m "add README"
	git push -u origin master

### Push an existing folder
	cd existing_folder
	git init
	git remote add origin https://gitlab.com/example/example
	
	git add .
	git commit -m "Initial commit"
	git push -u origin master

### Push an existing Git repository
	cd existing_repo
	git remote rename origin old-origin
	git remote add origin https://gitlab.com/AU_CTP/G4_TB.git

	git push -u origin --all
	git push -u origin --tags

### Old instructions
	git add "file to add"
	git commit -am "some message"
	git push
