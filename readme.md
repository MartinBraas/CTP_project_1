# Roomba, turtlebot3 navigation
### Group 2, CTP Project 1

## Introduction
The purpose behind the project, has been to use our knowledge of programming, as well as the knowledge gathered throughout the course, to implement a navigation algorithm, which enables a robot to navigate freely through a maze, as fast as possible, with as few collisions as possible.

## Requirements
- ROS (Noetic)
- Gazebo
- Catkin
- The following ROS packages
  - rospy
  - std_msgs
  - geometry_msgs
  - turtlebot3_simulations
  - turtlebot3-msgs
  - turtlebot3

## Installation
1. **Follow the ROS installation guide to install the full ROS package.**
  - Guide can be found here: http://wiki.ros.org/ROS/Installation
2. **Install Gazebo**
  - Installation can be found here: http://gazebosim.org/tutorials?cat=install

3. **Install Turtlebot 3 simulation packages**
  - Instructions can be found in the following links:
    - https://emanual.robotis.com/docs/en/platform/turtlebot3/quick-start/#pc-setup
    - https://emanual.robotis.com/docs/en/platform/turtlebot3/simulation/#gazebo-simulation
4. **Clone this repository into the src/ folder of your catkin workspace.**
5. **Run catkin_make from your catkin workspace.**


## Installation example on Ubuntu 20.04
**To install ROS Noetic and the following programs from the Ubuntu terminal, run the following commands.**

### Install ROS
	sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
	sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
	sudo apt update
	sudo apt install ros-noetic-desktop-full
	echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
	source ~/.bashrc
	sudo apt install python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential

### Install Gazebo
	curl -sSL http://get.gazebosim.org | sh

### Install Turtlebot3 simulation packages
	mkdir -p ~/catkin_ws/src
	cd ~/catkin_ws/src/
	git clone -b noetic-devel https://github.com/ROBOTIS-GIT/DynamixelSDK.git
	git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git
	git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3.git


### Build the packages
	cd ~/catkin_ws && catkin_make
	echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc


### Configuration
**Make sure the following lines are added  the bottom of your .bashrc:**
	source /opt/ros/noetic/setup.bash

	export OWN_IP=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | sed '2,$d') 
	export ROS_HOSTNAME=${OWN_IP}
	export ROS_MASTER_URI=http://${OWN_IP}:11311

	export IP_OF_TURTLEBOT=192.168.104.135
	alias setTurtlebot='export ROS_MASTER_URI=http://${IP_OF_TURTLEBOT}:11311'

	export TURTLEBOT3_MODEL=burger
	source ~/catkin_ws/devel/setup.bash

## Install the world/launch itself
**If you have a default installation by following the ROS tutorial, place the folder worlds_group2 inside ~/catkin_ws/src. Afterwards run**

	cd ~/catkin_ws
	catkin_make
	source devel/setup.bash

## Run
**Run our maze by excecuting the following command:**

**Map 1:**

	roslaunch worlds_group2 group_2_map_1.launch


**Map 2:**

	roslaunch worlds_group2 group_2_map_2.launch


**Map 3:**

	roslaunch worlds_group2 group_2_map_3.launch


**Map 4:**

	roslaunch worlds_group2 group_2_map_4.launch


## Navigation
**In a new terminal, run the navigation code.**

	python3 RoombaDrive.py
	
## Maintainers
Martin Braas Andreasen; martin.hatting@hotmail.com  
Steffen Torpe Simonsen; steffentorpe@gmail.com  
